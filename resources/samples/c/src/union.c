
/* Public Domain. All warranties are disclaimed. */

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

typedef union {
    struct {
	uint8_t al;
	uint8_t ah;
	uint8_t bl;
	uint8_t bh;
    } b;
    struct {
	uint16_t ax;
	uint16_t bx;
    } w;
} Regs;

static Regs regs;

int testUnion ( int n) {

    regs.w.ax = n;
    
    if (!regs.b.ah) {
	regs.b.bl = regs.b.al;
    }
    else {
	regs.b.bh = regs.b.al;
    }

    regs.b.al = n + 0x23;

    return (1);
}

int testUnionSplat ( Regs* regs) {

    if (!regs ->b.ah) {
	regs ->b.bl = regs ->b.al;
    }
    else {
	regs ->b.bh = regs ->b.al;
    }

    regs ->b.al += 0x23;

    return (regs ->w.bx);
}

int testUnionFlat ( Regs regs) {

    if (!regs.b.ah) {
	regs.b.bl = regs.b.al;
    }
    else {
	regs.b.bh = regs.b.al;
    }

    regs.b.al += 0x23;

    return (regs.w.bx);
}

int testUnionStack ( int n) {
    
    Regs regs;

    regs.w.ax = 0;
    regs.w.bx = 0;

    regs.w.ax = n;

    if (!regs.b.ah) {
	regs.b.bl = regs.b.al;
    }
    else {
	regs.b.bh = regs.b.al;
    }

    regs.b.al += 0x23;

    return (regs.w.bx);
}
