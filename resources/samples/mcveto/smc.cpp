//keywords smc 
#include "../include/reach.h"

int main(void)
{
  int f=0;
  unsigned short Num;
  if(f!=0) 
    goto __CHECK; // This is only to make the IDApro 
                  // recognize the code after the _asm as "code" and not data
  _asm {
     //mov ax, cs
     //push ds
     //mov ds, ax
     mov cx, 1;             
     inc byte ptr [cnt + 1];    /* Adding a "jmp cnt" between the  */ 
     jmp cnt;
cnt: mov ax, 0;                 /* INC and the MOV will cause the  */ 
     mov Num, cx;               /* modified code to be run         */ 
     //pop ds
  }
 __CHECK:
  if(Num == 1) {
    UNREACHABLE();//f = 7;
  }

  return 0;
}
