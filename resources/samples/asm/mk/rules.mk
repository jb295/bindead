
OBJDIR	= build/$objtype

OBJ	= ${SRC:%.asm=%.$O}
OBJ	= ${OBJ:%=$OBJDIR/%}

T	= ${SRC:%.asm=%.$objtype}
T	= ${T:%=$OBJDIR/%}

all:V: $OBJDIR $T

$OBJDIR:
	mkdir -p $OBJDIR

$OBJDIR/%.$objtype:Q: $OBJDIR/%.$O
	echo LD $target
	$CC $LDFLAGS -o $target $prereq

$OBJDIR/%.$O:Q: $SRCDIR/%.asm
        echo Fasm $target
	$ASM $prereq $target
	
clean:V:
	rm $OBJDIR/*.$O $T
